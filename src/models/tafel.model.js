const mongoose = require('mongoose')
const Schema = mongoose.Schema;
// const autoIncrement = require('mongoose-auto-increment');

const getModel = require('./model_cache')

// define the table schema
const TafelSchema = new Schema({
    tafelNummer: {
        type: Number,
        required: [true, 'Een tafel moet een tafelnummer hebben.']
    },
    aantalPersonen: {
        type: Number,
        required: [true, 'Een tafel moet een max aantal personen hebben.']
    }
}, {
    // include virtuals when serializing the schema to an object or JSON
    toObject: { virtuals: true },
    toJSON: { virtuals: true },
})

// TafelSchema.plugin(autoIncrement.plugin, 'Tafel');

// export the tafel model through a caching function
module.exports = getModel('Tafel', TafelSchema)
