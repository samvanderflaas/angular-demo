const express = require('express')
const router = express.Router()

const Tafel = require('../models/tafel.model')() // note we need to call the model caching function

const CrudController = require('../controllers/crud')

const tafelCrudController = new CrudController(Tafel)
    // const tafelController = require('../controllers/restaurant.controller')


// create a product
router.post('/', tafelCrudController.create)

// get all products
router.get('/', tafelCrudController.getAll)

// get a product
router.get('/:id', tafelCrudController.getOne)

// update a product
router.put('/:id', tafelCrudController.update)

// remove a product
router.delete('/:id', tafelCrudController.delete)

// purchase a product (not entirely restful *blush*)
// router.post('/:id/purchase', tafelController.purchase)

module.exports = router
