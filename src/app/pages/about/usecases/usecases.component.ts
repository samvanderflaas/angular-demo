import { Component, OnInit } from '@angular/core';
import { UseCase } from '../usecase.model';

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.css'],
})
export class UsecasesComponent implements OnInit {
  readonly PLAIN_USER = 'Reguliere gebruiker';
  readonly RESTAURANT_USER = 'Restaurant beheerder';
  readonly ADMIN_USER = 'Administrator';

  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Inloggen',
      description: 'Hiermee logt een bestaande gebruiker in.',
      scenario: [
        'Gebruiker vult email en password in en klikt op Login knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.',
      ],
      actor: this.PLAIN_USER,
      precondition: 'Geen',
      postcondition: 'De actor is ingelogd',
    },
    {
      id: 'UC-02',
      name: 'Overzicht reserveringen bekijken',
      description: 'Hiermee kan een ingelogde gebruiker zijn eigen reserveringen bekijken.',
      scenario: [
        'Gebruiker krijgt een overzicht van zijn reserveringen.',
      ],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'De reservering is geplaatst en toegevoegd aan de database.',
    },
    {
      id: 'UC-03',
      name: 'Overzicht reserveringen filteren op restaurant',
      description: 'Hiermee kan een ingelogde gebruiker zijn eigen reserveringen bekijken bij een specifiek restaurant.',
      scenario: [
        'Gebruiker kiest een restaurant uit de lijst van restaurants op het overzicht van reserveringen.',
        'De applicatie laat een overzicht van reserveringen zien bij het gekozen restaurant voor de ingelogde gebruiker.',
      ],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'De actor ziet een overzicht van al zijn reserveringen bij het gekozen restaurant.',
    },
    {
      id: 'UC-04',
      name: 'Overzicht reserveringen bekijken(als restaurant)',
      description: 'Hiermee kan een ingelogde restaurant beheerder zijn eigen reserveringen bekijken, of filteren op een specifieke dag en een specifiek tijdsblok.',
      scenario: [
        'De applicatie laat een overzicht van alle reserveringen zien van het restaurant van de ingelogde actor.',
        'De actor kiest een datum op een kalender en kiest mogelijk ook een tijdsblok',
        'De applicatie geeft een overzicht van alle reserveringen die overeenkomen met de gekozen filters.'
      ],
      actor: this.RESTAURANT_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'De actor ziet een overzicht van alle reserveringen die overeenkomen met de gekozen filters.',
    },
    {
      id: 'UC-05',
      name: 'Reservering plaatsen',
      description: 'Hiermee kan een ingelogde gebruiker een reservering plaatsen.',
      scenario: [
        'Gebruiker kiest een restaurant uit de lijst.',
        'Gebruiker kiest een tijdsblok.',
        'De applicatie checkt of er nog een tafel beschikbaar is op het geselecteerde tijdsblok.',
        'Indien er een tafel beschikbaar is wordt de reservering aangemaakt en deze wordt toegevoegd aan de database.',
        'De applicatie redirect naar het overzicht van de reserveringen van de gebruiker.'
      ],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'De reservering is geplaatst en toegevoegd aan de database.',
    },
    {
      id: 'UC-06',
      name: 'Reservering annuleren',
      description: 'Hiermee kan een ingelogde gebruiker een van zijn geplaatste reserveringen annuleren.',
      scenario: [
        'De gebruiker kiest een van geplaatste reserveringen uit zijn overzicht van geboekte reserveringen.',
        'De applicatie toont een bevestigingsscherm waar de gebruiker zijn keuze bevestigt.',
        'De reservering wordt verwijderd uit de database.',
      ],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'De gekozen reservering is verwijderd uit de database.',
    },
    {
      id: 'UC-07',
      name: 'Reservering annuleren als restauraunt beheerder',
      description: 'Hiermee kan een ingelogde restauraunt beheerder een van zijn reserveringen annuleren.',
      scenario: [
        'De actor kiest een van de geboekte reserveringen uit zijn overzicht van reserveringen.',
        'De actor klikt op de verwijderknop om de specifieke reservering te verwijderen.',
        'De applicatie toont een bevestigingsscherm waar de gebruiker zijn keuze bevestigt.',
        'De reservering wordt verwijderd uit de database.',
      ],
      actor: this.RESTAURANT_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'De gekozen reservering is verwijderd uit de database.',
    },
  ];

  constructor() {}

  ngOnInit(): void {}
}
