import { Injectable } from '@angular/core';
import { from, Observable, of } from 'rxjs';
import { delay, filter, mergeMap, take } from 'rxjs/operators';
import { Tafel } from './tafel.model';

@Injectable({
  providedIn: 'root'
})

export class TafelService {
  private tafels: Tafel[] = [
    {
      id: 1,
      tafelNummer: 1,
      aantalPersonen: 4,
      },
    {
      id: 2,
      tafelNummer: 2,
      aantalPersonen: 3,
      },
  ];

  constructor() {}

  getAll(): Observable<Tafel[]> {
    // 'of' maakt een Observable<User[]>
    return of(this.tafels).pipe();
  }

  getById(id: number): Observable<Tafel> {
    // 'from' maakt een array van Observable<User>
    // Van dat array willen we 1 waarde, met de gevraagde id.
    return from(this.tafels).pipe(
      filter((item) => item.id === id),
      take(1)
    );
  }

  delete(id: number): Observable<Tafel> {
    for(var i = 0; i<this.tafels.length; i++){
      if(this.tafels[i].id === id){
        this.tafels.splice(i, 1)
        return this.getById(id)
      }
    }
  }

  create(tafel: Tafel): Observable<Tafel> {
    tafel.id = this.tafels.length + 1
    this.tafels.push(tafel)
    return this.getById(tafel.id)
  }

  update(tafel: Tafel): Observable<Tafel> {
    for(var i = 0; i<this.tafels.length; i++){
      if(this.tafels[i].id === tafel.id){
        this.tafels.splice(i, 1, tafel)
        return this.getById(tafel.id)
      }
    }
  }
}
