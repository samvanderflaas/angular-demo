import { Component, OnDestroy, OnInit } from '@angular/core';
import { Tafel } from '../tafel.model';
import { TafelService } from '../tafel.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap, tap } from 'rxjs/operators';
import { of, Subscription } from 'rxjs';

@Component({
  selector: 'app-tafel-create',
  templateUrl: './tafel-create.component.html',
  styleUrls: ['./tafel-create.component.css']
})
export class TafelCreateComponent implements OnInit, OnDestroy {
  tafel: Tafel;
  subscription: Subscription;
  id: number;

  constructor(
    private tafelService: TafelService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.subscription = this.route.paramMap
      .pipe(
        tap(console.log),
        switchMap((params: ParamMap) => {
          if (!params.get('id')) {
            return of({
              id: 0,
              tafelNummer: 0,
              aantalPersonen: 0,
            })
          } else {
            return this.tafelService.getById(parseInt((params.get('id'))))
          }
        }),
        tap(console.log)
      )
      .subscribe((tafel) => {
        this.tafel = tafel;
        this.id = tafel.id !== 0 ? tafel.id : 'Nieuwe tafel';
      })
  }

  onSubmit(): void {
    console.log('onSubmit', this.tafel);

    if(this.tafel.id) {
      console.log('update tafel');
      this.tafelService
        .update(this.tafel)
        .subscribe((data) =>
          this.router.navigate(['..'], {relativeTo: this.route }));
    } else {
      console.log('create movie');
      this.tafelService
        .create(this.tafel)
        .subscribe((data) =>
          this.router.navigate(['..', data.id], {relativeTo: this.route }))
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
