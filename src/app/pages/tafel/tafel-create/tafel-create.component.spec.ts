import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TafelCreateComponent } from './tafel-create.component';

describe('TafelCreateComponent', () => {
  let component: TafelCreateComponent;
  let fixture: ComponentFixture<TafelCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TafelCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TafelCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
