import { TafelDetailComponent } from './tafel-detail/tafel-detail.component';
import { TafelListComponent } from './tafel-list/tafel-list.component';
import { TafelCreateComponent } from './tafel-create/tafel-create.component';

export const components: any[] = [
  TafelDetailComponent,
  TafelListComponent,
  TafelCreateComponent,
];

export * from './tafel-detail/tafel-detail.component';
export * from './tafel-list/tafel-list.component';
export * from './tafel-create/tafel-create.component';
