import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TafelListComponent } from './tafel-list.component';

describe('TafelListComponent', () => {
  let component: TafelListComponent;
  let fixture: ComponentFixture<TafelListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TafelListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TafelListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
