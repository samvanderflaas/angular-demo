import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Tafel } from '../tafel.model';
import { TafelService } from '../tafel.service';

@Component({
  selector: 'app-tafel-list',
  templateUrl: './tafel-list.component.html',
  styleUrls: ['./tafel-list.component.css'],
})
export class TafelListComponent implements OnInit {
  tafels$: Observable<Tafel[]>;

  constructor(private tafelService: TafelService) {}

  ngOnInit(): void {
    console.log('TafelList geladen');
    this.tafels$ = this.tafelService.getAll();
  }
}
