import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Tafel } from '../tafel.model';
import { TafelService } from '../tafel.service';
import { switchMap, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-tafel-detail',
  templateUrl: './tafel-detail.component.html',
  styleUrls: ['./tafel-detail.component.css'],
})
export class TafelDetailComponent implements OnInit, OnDestroy {
  tafel$: Observable<Tafel>;

  constructor(
    private tafelService: TafelService,
    private route: ActivatedRoute,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.tafel$ = this.route.paramMap.pipe(
      tap((params: ParamMap) => console.log('tafel.id = ', params.get('id'))),
      switchMap((params: ParamMap) =>
        this.tafelService.getById(parseInt(params.get('id'), 10))
      ),
      tap(console.log)
    );
  }

  delete(tafelid: number): void {
      console.log('deleting tafel with id: ', tafelid)
      this.tafelService.delete(tafelid)
      this.router.navigate(['..'], {relativeTo: this.route });
  }

  ngOnDestroy(): void {
    // Cleanup, niet nodig wanneer je geen subscribe gebruikt.
    // this.paramSubscription.unsubscribe();
  }
}
