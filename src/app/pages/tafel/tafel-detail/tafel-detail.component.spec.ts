import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TafelDetailComponent } from './tafel-detail.component';

describe('TafelDetailComponent', () => {
  let component: TafelDetailComponent;
  let fixture: ComponentFixture<TafelDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TafelDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TafelDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
